package com.projectdeep.tfvehicledetection.Activities;

import android.graphics.Bitmap;

import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import com.projectdeep.tfvehicledetection.CameraBase.CameraActivity;
import com.projectdeep.tfvehicledetection.Drawing.VehicleBorder;
import com.projectdeep.tfvehicledetection.ObjectTracking.ObjectTracking;
import com.projectdeep.tfvehicledetection.ObjectTracking.TrackingWithFeatureDetection;
import com.projectdeep.tfvehicledetection.ObjectTracking.TrackingWithOpticalFlow;
import com.projectdeep.tfvehicledetection.ObjectTracking.TrackingWithTemplateMatching;


/**
 * Created by kubilaykarapinar on 11.06.18.
 */

public class TrackingActivity extends CameraActivity {

    private static final String TAG = "TrackingActivity";
    private int MATCHING_TYPE;

    private ObjectTracking objectTracking;

    private int objectChosenState;
    private float xStart;
    private float yStart;
    private float xEnd;
    private float yEnd;

    Thread thread;
    boolean inProcess;

    int width;
    int height;

    private Rect rect;
    VehicleBorder vehicleBorder;

    Bitmap template;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vehicleBorder = new VehicleBorder();
        objectChosenState = 0;

    }

    @Override
    public void onResume() {
        super.onResume();

        Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);
        width = size.x;
        height = size.y;


        MATCHING_TYPE = ObjectTracking.OPTICAL_FLOW;

        switch (MATCHING_TYPE) {
            case ObjectTracking.FEATURE_MATCHING:
                objectTracking = new TrackingWithFeatureDetection(width, height);
                break;
            case ObjectTracking.TEMPLATE_MATCHING:
                objectTracking = new TrackingWithTemplateMatching(width, height);
                break;
            case ObjectTracking.OPTICAL_FLOW:
                objectTracking = new TrackingWithOpticalFlow(width, height, getDrawingView());
                break;
        }


        inProcess = false;

        getDrawingView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                float x = event.getX();
                float y = event.getY();

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:

                        xStart = x;
                        yStart = y;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        rect = new Rect((int) xStart, (int) yStart, (int) x, (int) y);
                        getDrawingView().clearView();
                        getDrawingView().getCanvas().drawRect(rect, vehicleBorder.getPaint());
                        break;
                    case MotionEvent.ACTION_UP:
                        xEnd = x;
                        yEnd = y;
                        rect = new Rect((int) xStart, (int) yStart, (int) xEnd, (int) yEnd);
                        getDrawingView().clearView();
                        objectChosenState = 1;
                        break;
                }
                return true;
            }
        });
    }


    @Override
    public void doAfterFrame(final Bitmap bitmap) {

        thread = new Thread() {
            @Override
            public void run() {

                if (!inProcess) {
                    inProcess = true;
                    matching(bitmap);
                    inProcess = false;


                }
            }
        };
        thread.start();
    }

    public void matching(final Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        if (objectChosenState == 0) {


        } else if (objectChosenState == 1) {

            int b_startX = (int) (xStart * w / width);
            int b_startY = (int) (yStart * h / height);
            int b_endX = (int) (xEnd * w / width);
            int b_endY = (int) (yEnd * h / height);

            int temp;
            if (b_startX > b_endX) {
                temp = b_startX;
                b_startX = b_endX;
                b_endX = temp;

            }

            if (b_startY > b_endY) {
                temp = b_startY;
                b_startY = b_endY;
                b_endY = temp;

            }


            int b_width = b_endX - b_startX;
            int b_height = b_endY - b_startY;

            if (b_height != 0 && b_width != 0) {
                template = Bitmap.createBitmap(bitmap, b_startX, b_startY, b_width, b_height);

                objectTracking.initializeTemplateForTracking(template, new Rect(b_startX, b_startY, b_endX, b_endY));
                objectChosenState = 2;
            }

        } else if (objectChosenState == 2) {

            Rect match_rect = objectTracking.getObjectLocation(bitmap);
            getDrawingView().clearView();
            getDrawingView().getCanvas().drawRect(match_rect, vehicleBorder.getPaint());

        }

    }

}


