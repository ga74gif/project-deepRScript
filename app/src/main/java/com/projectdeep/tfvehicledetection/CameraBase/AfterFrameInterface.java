package com.projectdeep.tfvehicledetection.CameraBase;

import android.graphics.Bitmap;

/**
 * Created by kubilaykarapinar on 11.06.18.
 */

public interface AfterFrameInterface {
    void doAfterFrame(Bitmap bitmap);
}
