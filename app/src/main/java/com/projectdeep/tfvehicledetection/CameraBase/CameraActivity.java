package com.projectdeep.tfvehicledetection.CameraBase;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.NumberPicker;

import com.projectdeep.tfvehicledetection.Drawing.DrawingView;
import com.projectdeep.tfvehicledetection.R;

public class CameraActivity extends AppCompatActivity implements AfterFrameInterface {


    private static final int PERMISSIONS_REQUEST = 1;
    private CameraFragment cameraFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        if (hasPermission()) {
            setCameraFragment();
        } else {
            requestPermission();
        }

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {

        super.onPause();
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {

            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE

                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
            );

            //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            // | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }
    }


    private boolean hasPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(
            final int requestCode, final String[] permissions, final int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                setCameraFragment();
            } else {
                requestPermission();
            }
        }
    }

    private void setCameraFragment() {

        cameraFragment = CameraFragment.newInstance();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, cameraFragment)
                .commit();


    }

    public DrawingView getDrawingView() {
        DrawingView drawingView = cameraFragment.getDrawingView();
        return drawingView;

    }


    @Override
    public void doAfterFrame(Bitmap bitmap) {

    }
}