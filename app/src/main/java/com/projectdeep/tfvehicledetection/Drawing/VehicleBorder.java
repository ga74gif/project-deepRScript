package com.projectdeep.tfvehicledetection.Drawing;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by kubilaykarapinar on 08.06.18.
 */

public class VehicleBorder {
    private Paint paint;

    public VehicleBorder() {
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(3);
    }

    public Paint getPaint() {
        return paint;
    }
}
