package com.projectdeep.tfvehicledetection.ObjectTracking;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by kubilaykarapinar on 12.06.18.
 */

public abstract class ObjectTracking {

    public static final int FEATURE_MATCHING = 1;
    public static final int TEMPLATE_MATCHING = 2;
    public static final int OPTICAL_FLOW = 3;


    int width;
    int height;


    protected Bitmap template;
    protected Rect templateLocation;


    public ObjectTracking(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public abstract Rect getObjectLocation(Bitmap bitmap);

    public abstract void initializeTemplateForTracking(Bitmap bitmap, Rect templateLocation);


}
