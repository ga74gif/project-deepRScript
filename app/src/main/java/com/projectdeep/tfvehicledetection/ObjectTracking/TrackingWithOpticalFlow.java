package com.projectdeep.tfvehicledetection.ObjectTracking;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

import com.projectdeep.tfvehicledetection.Drawing.DrawingView;
import com.projectdeep.tfvehicledetection.Drawing.VehicleBorder;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;

import java.util.List;

/**
 * Created by kubilaykarapinar on 17.06.18.
 */

public class TrackingWithOpticalFlow extends ObjectTracking {

    private int mViewMode;
    private Mat mRgba;
    private Mat mIntermediateMat;
    private Mat mGray;
    private Mat mPrevGray;

    MatOfPoint2f prevFeatures, nextFeatures;
    MatOfPoint features;

    MatOfByte status;
    MatOfFloat err;

    DrawingView drawingView;
    VehicleBorder vehicleBorder;

    public TrackingWithOpticalFlow(int width, int height) {
        super(width, height);
    }

    public TrackingWithOpticalFlow(int width, int height, DrawingView drawingView) {
        super(width, height);
        vehicleBorder = new VehicleBorder();
        this.drawingView = drawingView;
    }


    @Override
    public Rect getObjectLocation(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Mat mGray = new Mat();
        Utils.bitmapToMat(bitmap, mGray);


        if (features.toArray().length == 0) {
            int rowStep = 50, colStep = 25;
            int nRows = (templateLocation.height() / rowStep) + 1, nCols = (templateLocation.width() / colStep) + 1;

            Point points[] = new Point[(nRows) * (nCols)];
            for (int i = 0; i < nRows; i++) {
                for (int j = 0; j < nCols; j++) {
                    points[i * nCols + j] = new Point(templateLocation.left + j * colStep, templateLocation.top + i * rowStep);
                }
            }

            features.fromArray(points);

            prevFeatures.fromList(features.toList());
            mPrevGray = mGray.clone();
        }

        nextFeatures.fromArray(prevFeatures.toArray());
        Video.calcOpticalFlowPyrLK(mPrevGray, mGray,
                prevFeatures, nextFeatures, status, err);

        List<Point> prevList = features.toList(), nextList = nextFeatures.toList();
        Scalar color = new Scalar(255, 0, 0);

        float h_scale = (float) height / (float) h;
        float w_scale = (float) width / (float) w;


        float sum_diff_x = 0;
        float sum_diff_y = 0;
        int counter = 0;


        //Todo: Instead of avaraging find new pixels and get convex hull

        for (int i = 0; i < prevList.size(); i++) {
            //Imgproc.line(mGray, prevList.get(i), nextList.get(i), color);


            if (isInsideRect(templateLocation, (float) prevList.get(i).x, (float) prevList.get(i).y)
                    && isInsideRect(templateLocation, (float) nextList.get(i).x, (float) nextList.get(i).y)) {


                sum_diff_x += (float) nextList.get(i).x - (float) prevList.get(i).x;
                sum_diff_y += (float) nextList.get(i).y - (float) prevList.get(i).y;
                counter++;
            }

        }

        mPrevGray = mGray.clone();


        if (counter < 20) {
            float avg_diff_x = sum_diff_x / counter;
            float avg_diff_y = sum_diff_y / counter;

            Rect match_rect = new Rect((int) (templateLocation.left * 1) + (int) avg_diff_x, (int) (templateLocation.top * 1) + (int) avg_diff_y,
                    (int) (templateLocation.right * 1) + (int) avg_diff_x, (int) (templateLocation.bottom * 1) + (int) avg_diff_y);


            templateLocation = match_rect;
            return new Rect((int) (templateLocation.left * w_scale), (int) (templateLocation.top * h_scale),
                    (int) (templateLocation.right * w_scale), (int) (templateLocation.bottom * h_scale));
        } else {
            return new Rect(0, 0, 0, 0);
        }


    }

    private boolean isInsideRect(Rect rect, float x, float y) {

        return (rect.left < x && x < rect.right && rect.top < y && y < rect.bottom);

    }

    @Override
    public void initializeTemplateForTracking(Bitmap bitmap, Rect templateLocation) {
        template = bitmap;
        this.templateLocation = templateLocation;
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mIntermediateMat = new Mat(height, width, CvType.CV_8UC4);
        mGray = new Mat(height, width, CvType.CV_8UC1);
        resetVars();


    }


    private void resetVars() {
        mPrevGray = new Mat(mGray.rows(), mGray.cols(), CvType.CV_8UC1);
        features = new MatOfPoint();
        prevFeatures = new MatOfPoint2f();
        nextFeatures = new MatOfPoint2f();
        status = new MatOfByte();
        err = new MatOfFloat();
    }
}
