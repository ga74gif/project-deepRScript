package com.projectdeep.tfvehicledetection.ObjectTracking;

import android.graphics.Bitmap;
import android.graphics.Rect;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

/**
 * Created by kubilaykarapinar on 12.06.18.
 */

public class TrackingWithTemplateMatching extends ObjectTracking {


    public TrackingWithTemplateMatching(int width, int height) {
        super(width, height);
    }

    @Override
    public Rect getObjectLocation(Bitmap bitmap) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int match_method = Imgproc.TM_CCOEFF;

        Mat imageMat = new Mat();
        Mat templateMat = new Mat();
        Utils.bitmapToMat(bitmap, imageMat);
        Utils.bitmapToMat(template, templateMat);


        // Create the result matrix
        int result_cols = imageMat.cols() - templateMat.cols() + 1;
        int result_rows = imageMat.rows() - templateMat.rows() + 1;
        Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

        // / Do the Matching and Normalize
        Imgproc.matchTemplate(imageMat, templateMat, result, match_method);
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

        // / Localizing the best match with minMaxLoc
        Core.MinMaxLocResult mmr = Core.minMaxLoc(result);

        org.opencv.core.Point matchLoc;
        if (match_method == Imgproc.TM_SQDIFF || match_method == Imgproc.TM_SQDIFF_NORMED) {
            matchLoc = mmr.minLoc;
        } else {
            matchLoc = mmr.maxLoc;
        }

        int x_s_coord = (int) (matchLoc.x * width / w);
        int y_s_coord = (int) (matchLoc.y * height / h);
        int width_rect = (int) (template.getWidth() * width / w);
        int height_rect = (int) (template.getHeight() * height / h);

        Rect match_rect = new Rect(x_s_coord, y_s_coord, x_s_coord + width_rect, y_s_coord + height_rect);
        return match_rect;
    }

    @Override
    public void initializeTemplateForTracking(Bitmap bitmap, Rect templateLocation) {
        template = bitmap;
    }
}
