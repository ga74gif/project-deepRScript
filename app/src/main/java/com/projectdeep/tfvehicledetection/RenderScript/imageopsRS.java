package com.projectdeep.tfvehicledetection.RenderScript;

import android.graphics.Bitmap;
import android.renderscript.*;


public class imageopsRS {
    public static Bitmap bmpresize(RenderScript rs, Bitmap src, int destW, int destH) {
        Bitmap.Config bmpCfg = src.getConfig();
        Allocation tmp = Allocation.createFromBitmap(rs, src);
        Allocation tmpType = Allocation.createTyped(rs, tmp.getType());
        tmp.destroy();
        Bitmap dst = Bitmap.createBitmap(destW, destH, bmpCfg);
        Type t = Type.createXY(rs, tmpType.getElement(), destW, destH);
        Allocation tmpO = Allocation.createTyped(rs, t);
        ScriptIntrinsicResize rsIntr = ScriptIntrinsicResize.create(rs);
        rsIntr.setInput(tmpType);
        rsIntr.forEach_bicubic(tmpO);
        tmpO.copyTo(dst);
        tmpType.destroy();
        tmpO.destroy();
        rsIntr.destroy();

        return dst;
    }


    public Bitmap YUV2RGB(RenderScript rs,byte[] yuvByteArray,int W,int H) {
        ScriptIntrinsicYuvToRGB yuvToRgbIntrinsic = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));
        Type.Builder yuvType = new Type.Builder(rs, Element.U8(rs)).setX(yuvByteArray.length);
        Allocation in = Allocation.createTyped(rs, yuvType.create(), Allocation.USAGE_SCRIPT);
        Type.Builder rgbaType = new Type.Builder(rs, Element.RGBA_8888(rs)).setX(W).setY(H);
        Allocation out = Allocation.createTyped(rs, rgbaType.create(), Allocation.USAGE_SCRIPT);
        in.copyFrom(yuvByteArray);
        yuvToRgbIntrinsic.setInput(in);
        yuvToRgbIntrinsic.forEach(out);
        Bitmap bmp = Bitmap.createBitmap(W, H, Bitmap.Config.ARGB_8888);
        out.copyTo(bmp);
        yuvToRgbIntrinsic.destroy();
        rs.destroy();
        return bmp;
    }
}