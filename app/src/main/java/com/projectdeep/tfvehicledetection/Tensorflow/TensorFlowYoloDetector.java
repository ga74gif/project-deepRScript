package com.projectdeep.tfvehicledetection.Tensorflow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Log;

import com.projectdeep.tfvehicledetection.Utils.MathUtils;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by kubilaykarapinar on 20.06.18.
 */

public class TensorFlowYoloDetector {

    // Pre-allocated buffers.
    private int[] intValues;

    private float[] floatValues;


    private TensorFlowInferenceInterface inferenceInterface;

    public TensorFlowYoloDetector(Context context) {

        intValues = new int[Config.INPUT_SIZE * Config.INPUT_SIZE];

        floatValues = new float[Config.INPUT_SIZE * Config.INPUT_SIZE * 3];

        inferenceInterface = new TensorFlowInferenceInterface(context.getAssets(), Config.MODEL_FILE);
    }


    public List<Recognition> recognizeImage(final Bitmap bitmap) {


        // Preprocess the image data from 0-255 int to normalized float based
        // on the provided parameters.

        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());


        int intValuesSize = intValues.length;
        for (int i = 0; i < intValuesSize; ++i) {

            int index = 3 * i;
            int value = intValues[i];
            floatValues[index + 0] = ((value >> 16) & 0xFF) / 255.0f;
            floatValues[index + 1] = ((value >> 8) & 0xFF) / 255.0f;
            floatValues[index + 2] = (value & 0xFF) / 255.0f;

        }


        // Copy the input data into TensorFlow.
        inferenceInterface.feed(Config.INPUT_NAME, floatValues, 1, Config.INPUT_SIZE, Config.INPUT_SIZE, 3);


        // Run the inference call.
        inferenceInterface.run(new String[]{Config.OUTPUT_NAME});

        // Copy the output Tensor back into the output array.
        final int gridWidth = bitmap.getWidth() / Config.YOLO_BLOCK_SIZE;
        final int gridHeight = bitmap.getHeight() / Config.YOLO_BLOCK_SIZE;
        final float[] output =
                new float[gridWidth * gridHeight * (Config.NUM_CLASSES + 5) * Config.NUM_BOXES_PER_BLOCK];


        inferenceInterface.fetch(Config.OUTPUT_NAME, output);

        // Find the best detections.
        final PriorityQueue<Recognition> pq =
                new PriorityQueue<Recognition>(
                        1,
                        new Comparator<Recognition>() {
                            @Override
                            public int compare(final Recognition lhs, final Recognition rhs) {
                                // Intentionally reversed to put high confidence at the head of the queue.
                                return Float.compare(rhs.getConfidence(), lhs.getConfidence());
                            }
                        });

        for (int y = 0; y < gridHeight; ++y) {
            for (int x = 0; x < gridWidth; ++x) {
                for (int b = 0; b < Config.NUM_BOXES_PER_BLOCK; ++b) {
                    final int offset =
                            (gridWidth * (Config.NUM_BOXES_PER_BLOCK * (Config.NUM_CLASSES + 5))) * y
                                    + (Config.NUM_BOXES_PER_BLOCK * (Config.NUM_CLASSES + 5)) * x
                                    + (Config.NUM_CLASSES + 5) * b;

                    final float xPos = (x + MathUtils.expit(output[offset + 0])) * Config.YOLO_BLOCK_SIZE;
                    final float yPos = (y + MathUtils.expit(output[offset + 1])) * Config.YOLO_BLOCK_SIZE;

                    final float w = (float) (Math.exp(output[offset + 2]) * Config.ANCHORS[2 * b + 0]) * Config.YOLO_BLOCK_SIZE;
                    final float h = (float) (Math.exp(output[offset + 3]) * Config.ANCHORS[2 * b + 1]) * Config.YOLO_BLOCK_SIZE;

                    final RectF rect =
                            new RectF(
                                    Math.max(0, xPos - w / 2),
                                    Math.max(0, yPos - h / 2),
                                    Math.min(bitmap.getWidth() - 1, xPos + w / 2),
                                    Math.min(bitmap.getHeight() - 1, yPos + h / 2));
                    final float confidence = MathUtils.expit(output[offset + 4]);

                    int detectedClass = -1;
                    float maxClass = 0;

                    final float[] classes = new float[Config.NUM_CLASSES];
                    for (int c = 0; c < Config.NUM_CLASSES; ++c) {
                        classes[c] = output[offset + 5 + c];
                    }
                    MathUtils.softmax(classes);

                    for (int c = 0; c < Config.NUM_CLASSES; ++c) {
                        if (classes[c] > maxClass) {
                            detectedClass = c;
                            maxClass = classes[c];
                        }
                    }

                    final float confidenceInClass = maxClass * confidence;
                    if (confidenceInClass > Config.MINIMUM_CONFIDENCE_YOLO) {

                        pq.add(new Recognition(offset, Config.LABELS[detectedClass], confidenceInClass, rect));
                    }
                }
            }
        }


        for (Recognition r1 : pq) {
            for (Recognition r2 : pq) {
                if (!r1.equals(r2)) {
                    if (r1.getLocation().intersect(r2.getLocation())) {
                        r1.getLocation().union(r2.getLocation());
                        r1.setConfidence(Math.max(r1.getConfidence(), r2.getConfidence()));
                        pq.remove(r2);
                        Log.d("Merged", "Merged..");
                    }
                }
            }
        }


        final ArrayList<Recognition> recognitions = new ArrayList<Recognition>();
        for (int i = 0; i < Math.min(pq.size(), Config.MAX_RESULTS); ++i) {
            recognitions.add(pq.poll());
        }

        return recognitions;
    }


    public String getStatString() {
        return inferenceInterface.getStatString();
    }

    public void close() {
        inferenceInterface.close();
    }
}
